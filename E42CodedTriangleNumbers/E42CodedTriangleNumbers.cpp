// E42CodedTriangleNumbers.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <fstream>
#include <vector>
#include <string>
#include <iostream>
#include <sstream>
#include <algorithm>
using std::ifstream;
using std::ofstream;
using std::vector;
using std::string;
using std::endl;
using std::cout;
using std::stringstream;
using std::transform;


int getWords(vector<string>& words) {
	int maxSize = 0;
	ifstream ifs{ "p042_words.txt" };
	stringstream ss;
	ss << ifs.rdbuf();
	string sfile = ss.str();
	size_t i = 1;
	while(true){
		string word;
		while (sfile[i] != '\"') {
			word += sfile.substr(i, 1);
			++i;
		}
		if (word.size() > maxSize)
			maxSize = word.size();
		words.push_back(word);
		++i; //ignore "
		if (i == sfile.size())
			break;
		else
			i+=2; //ignore ,"
	}
	return maxSize;
}
vector<size_t> getTriangleNumbers(size_t max){
	vector<size_t> triangles;
	triangles.push_back(0);
	size_t i = 1;
	size_t value = 0;
	while(value < max) {
		triangles.push_back( value = (i * (i + 1)) / 2);
		++i;
	}
	return triangles;
}
size_t getValue(char c) {
	return static_cast<size_t>(c) - 64;
}
int main()
{
	vector<string> words;
	int max = getWords(words);
	auto triangleSizes = getTriangleNumbers(max*getValue('Z'));
	vector<size_t> values(words.size());
	std::transform(words.begin(), words.end(), values.begin(), 
	[](string s)->size_t
	{
		size_t v = 0;
		for (char c : s) {
			v += getValue(c);
		}
		return v;
	}
	);
	cout << std::count_if(values.begin(),values.end(),
		[&triangleSizes](size_t i)->bool
	{
		return triangleSizes.end() != std::find(triangleSizes.begin()+1, triangleSizes.end(), i);
	}
	);
    return 0;
}

//for (size_t i : vec) {
//	cout << i << "  ";
//}
//for (string w : words) {
//	cout << w << " ";
//}
